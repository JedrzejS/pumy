package szewczyk.pum.quiz;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Build;
import android.widget.Toast;

import androidx.annotation.Nullable;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import szewczyk.pum.quiz.QuizContract.*;

public class QuizDbHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "Quiz.db";
    private static final int DATABASE_VER = 3;

    private SQLiteDatabase db;

    public QuizDbHelper(@Nullable Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VER);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        this.db = db;

        final String SQL_CREATE_QUESTIONS_TABLE = "CREATE TABLE " +
                QuestionsTable.TABLE_NAME + "(" +
                QuestionsTable._ID + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                QuestionsTable.COLUMN_QUESTION + " TEXT, " +
                QuestionsTable.COLUMN_OPTION1 + " TEXT, " +
                QuestionsTable.COLUMN_OPTION2 + " TEXT, " +
                QuestionsTable.COLUMN_OPTION3 + " TEXT, " +
                QuestionsTable.COLUMN_ANSWER + " INTEGER, " +
                QuestionsTable.COLUMN_DIFFICULTY + " TEXT" +
                ")";
        db.execSQL(SQL_CREATE_QUESTIONS_TABLE);
        _fillQuestionsTable();

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + QuestionsTable.TABLE_NAME);
        onCreate(db);
    }

    private void _fillQuestionsTable(){
        QuestionModel q1 = new QuestionModel("(EASY) A correct","A", "B", "C", 1, QuestionModel.DIFFICULTY_EASY);
        addQuestion(q1);
        QuestionModel q2 = new QuestionModel("(MEDIUM) B correct","A", "B", "C", 2, QuestionModel.DIFFICULTY_MEDIUM);
        addQuestion(q2);
        QuestionModel q3 = new QuestionModel("(MEDIUM) A correct","A", "B", "C", 1, QuestionModel.DIFFICULTY_MEDIUM);
        addQuestion(q3);
        QuestionModel q4 = new QuestionModel("(HARD) B correct","A", "B", "C", 2, QuestionModel.DIFFICULTY_HARD);
        addQuestion(q4);
        QuestionModel q5 = new QuestionModel("(HARD) C correct","A", "B", "C", 3, QuestionModel.DIFFICULTY_HARD);
        addQuestion(q5);
        QuestionModel q6 = new QuestionModel("(EASY) B correct","A", "B", "C", 2, QuestionModel.DIFFICULTY_EASY);
        addQuestion(q6);
        QuestionModel q7 = new QuestionModel("(EASY) C correct","A", "B", "C", 3, QuestionModel.DIFFICULTY_EASY);
        addQuestion(q7);
        QuestionModel q8 = new QuestionModel("(MEDIUM) C correct","A", "B", "C", 3, QuestionModel.DIFFICULTY_MEDIUM);
        addQuestion(q8);
        QuestionModel q9 = new QuestionModel("(HARD) C correct","A", "B", "C", 3, QuestionModel.DIFFICULTY_HARD);
        addQuestion(q9);
    }
    private void addQuestion(QuestionModel qm){
        ContentValues cv = new ContentValues();
        cv.put(QuestionsTable.COLUMN_QUESTION, qm.getQuestion());
        cv.put(QuestionsTable.COLUMN_OPTION1, qm.getOption1());
        cv.put(QuestionsTable.COLUMN_OPTION2, qm.getOption2());
        cv.put(QuestionsTable.COLUMN_OPTION3, qm.getOption3());
        cv.put(QuestionsTable.COLUMN_ANSWER, qm.getCorrectAnswer());
        cv.put(QuestionsTable.COLUMN_DIFFICULTY, qm.getDifficulty());
        db.insert(QuestionsTable.TABLE_NAME, null, cv);
    }
    public List<QuestionModel> getAllQuestions(){
        List<QuestionModel> qList = new ArrayList<>();
        db = getReadableDatabase();
        Cursor c = db.rawQuery("SELECT * FROM "+ QuestionsTable.TABLE_NAME + " LIMIT 5",null);
        if(c.moveToFirst()){
            do{
                QuestionModel q = new QuestionModel();
                q.setQuestion(c.getString((c.getColumnIndex(QuestionsTable.COLUMN_QUESTION))));
                q.setOption1(c.getString((c.getColumnIndex(QuestionsTable.COLUMN_OPTION1))));
                q.setOption2(c.getString((c.getColumnIndex(QuestionsTable.COLUMN_OPTION2))));
                q.setOption3(c.getString((c.getColumnIndex(QuestionsTable.COLUMN_OPTION3))));
                q.setCorrectAnswer(c.getInt((c.getColumnIndex(QuestionsTable.COLUMN_ANSWER))));
                q.setDifficulty(c.getString((c.getColumnIndex(QuestionsTable.COLUMN_DIFFICULTY))));
                qList.add(q);
            } while(c.moveToNext());
        }
        c.close();
        return qList;
    }
    public List<QuestionModel> getQuestions(String difficulty){
        List<QuestionModel> qList = new ArrayList<>();
        db = getReadableDatabase();
        String[] selectionArgs = new String[]{difficulty};
        Cursor c = db.rawQuery("SELECT * FROM "+ QuestionsTable.TABLE_NAME + " WHERE "+ QuestionsTable.COLUMN_DIFFICULTY +" = ? LIMIT 5",selectionArgs);
        if(c.moveToFirst()){
            do{
                QuestionModel q = new QuestionModel();
                q.setQuestion(c.getString((c.getColumnIndex(QuestionsTable.COLUMN_QUESTION))));
                q.setOption1(c.getString((c.getColumnIndex(QuestionsTable.COLUMN_OPTION1))));
                q.setOption2(c.getString((c.getColumnIndex(QuestionsTable.COLUMN_OPTION2))));
                q.setOption3(c.getString((c.getColumnIndex(QuestionsTable.COLUMN_OPTION3))));
                q.setCorrectAnswer(c.getInt((c.getColumnIndex(QuestionsTable.COLUMN_ANSWER))));
                q.setDifficulty(c.getString((c.getColumnIndex(QuestionsTable.COLUMN_DIFFICULTY))));
                qList.add(q);
            } while(c.moveToNext());
        }
        c.close();
        return qList;
    }

    public void addQuestion(String qestion, String answer1, String answer2, String answer3, int correctAnswerNr, String difficulty){
        String[] availableDifficulties = new String[]{"Easy","Medium","Hard"};
        if(Arrays.asList(availableDifficulties).contains(difficulty) && correctAnswerNr < 4 && correctAnswerNr > 0)
        {
            db = getReadableDatabase();
            Cursor c = db.rawQuery("INSERT INTO "+ QuestionsTable.TABLE_NAME + " VALUES("+
                    qestion + ", "+
                    answer1 + ", "+
                    answer2 + ", "+
                    answer3 + ", "+
                    correctAnswerNr + ", "+
                    difficulty + ")",null);
        }

    }
}
