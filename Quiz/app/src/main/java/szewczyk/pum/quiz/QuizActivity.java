package szewczyk.pum.quiz;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.view.View;
import android.widget.Button;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.util.Collections;
import java.util.List;
import java.util.Locale;

public class QuizActivity extends AppCompatActivity {
    public static final String EXTRA_SCORE = "extraScore";

    private static final long COUNTDOWN_IN_MILS = 30000; // 30 sec

    private TextView text_question;
    private TextView current_score_text;
    private TextView question_number_text;
    private TextView difficulty;
    private TextView timer_text;
    private RadioGroup rbGroup;
    private RadioButton rb1;
    private RadioButton rb2;
    private RadioButton rb3;
    private Button buttonNext;

    private ColorStateList textColorDefaultRb; //radio buttons
    private ColorStateList textColorDefaultCd; // timer

    private CountDownTimer countDownTimer;
    private long timeLeftinMils;

    private List<QuestionModel> qList;
    private int questionCounter;
    private int questionCountTotal;
    private QuestionModel currentQuestion;

    private int score;
    private boolean answered;
    private long backPressedTime;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_quiz);

        text_question = findViewById(R.id.text_question);
        current_score_text = findViewById(R.id.current_score_text);
        question_number_text = findViewById(R.id.question_number_text);
        difficulty = findViewById(R.id.text_view_difficulty);
        timer_text = findViewById(R.id.timer_text);
        rbGroup = findViewById(R.id.radio_group);
        rb1 = findViewById(R.id.radio_button_1);
        rb2 = findViewById(R.id.radio_button_2);
        rb3 = findViewById(R.id.radio_button_3);
        buttonNext = findViewById(R.id.next_button);

        textColorDefaultRb = rb1.getTextColors();
        textColorDefaultCd = timer_text.getTextColors();

        Intent intent = getIntent();
        String difficultyString = intent.getStringExtra(MainActivity.EXTRA_DIFICULTY);
        difficulty.setText("Difficulty: "+difficultyString);

        QuizDbHelper dbHelper = new QuizDbHelper(this);
        //qList = dbHelper.getAllQuestions();
        qList = dbHelper.getQuestions(difficultyString);

        questionCountTotal = qList.size();
        Collections.shuffle(qList);

        showNextQuestion();

        buttonNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!answered){
                    if(rb1.isChecked() || rb2.isChecked() || rb3.isChecked()){
                        checkAnswer();
                    }
                    else{
                        Toast.makeText(QuizActivity.this, "Please select answer",Toast.LENGTH_LONG);
                    }
                }
                else{
                    showNextQuestion();
                }
            }
        });
    }

    private void showNextQuestion(){
        rb1.setTextColor(textColorDefaultRb);
        rb2.setTextColor(textColorDefaultRb);
        rb3.setTextColor(textColorDefaultRb);
        rbGroup.clearCheck();

        if(questionCounter < questionCountTotal){
            currentQuestion = qList.get(questionCounter);

            text_question.setText(currentQuestion.getQuestion());
            rb1.setText(currentQuestion.getOption1());
            rb2.setText(currentQuestion.getOption2());
            rb3.setText(currentQuestion.getOption3());

            questionCounter++;
            question_number_text.setText("Question: "+questionCounter +"/"+questionCountTotal);
            answered = false;
            buttonNext.setText("Confirm");

            timeLeftinMils = COUNTDOWN_IN_MILS;
            startCountDown();
        }
        else{
            finishQuiz();
        }
    }

    private void startCountDown(){
        countDownTimer = new CountDownTimer(timeLeftinMils, 1000) { // tika co 1 sec
            @Override
            public void onTick(long millisUntilFinished) {
                timeLeftinMils = millisUntilFinished;
                updateCountdownText();
            }

            @Override
            public void onFinish() {
                timeLeftinMils = 0;
                updateCountdownText();
                checkAnswer(); // po koncu odliczania zaakcpetuje zaznaczona odpowiedz
            }
        }.start();
    }

    private void updateCountdownText(){
        int sec = (int)(timeLeftinMils / 1000);
        String timeFormated = String.format(Locale.getDefault(), "%02d", sec);
        timer_text.setText(timeFormated);

        if(timeLeftinMils < 10000){
            timer_text.setTextColor(Color.RED);
        }
        else
            timer_text.setTextColor(textColorDefaultCd);
    }

    private void checkAnswer(){
        answered = true;

        countDownTimer.cancel();

        RadioButton rbSelected = findViewById(rbGroup.getCheckedRadioButtonId());
        int answerNr = rbGroup.indexOfChild(rbSelected) + 1;

        if(answerNr == currentQuestion.getCorrectAnswer()){
            score++;
            current_score_text.setText("Score: "+score);
        }
        showSolution();
    }

    private void showSolution(){
        rb1.setTextColor(Color.RED);
        rb2.setTextColor(Color.RED);
        rb3.setTextColor(Color.RED);

        switch(currentQuestion.getCorrectAnswer()){
            case 1:
                rb1.setTextColor(Color.GREEN);
                text_question.setText("Answer 1 is correct");
                break;
            case 2:
                rb2.setTextColor(Color.GREEN);
                text_question.setText("Answer 2 is correct");
                break;
            case 3:
                rb3.setTextColor(Color.GREEN);
                text_question.setText("Answer 3 is correct");
                break;
        }
        if(questionCounter < questionCountTotal){
            buttonNext.setText("Next");
        }
        else
            buttonNext.setText("Finish");
    }

    private void finishQuiz(){
        Intent resultIntent = new Intent();
        resultIntent.putExtra(EXTRA_SCORE, score);
        setResult(RESULT_OK, resultIntent);
        finish();
    }

    @Override
    public void onBackPressed() {
        if(backPressedTime + 2000 > System.currentTimeMillis()){
            finish();
        }
        else{
            Toast.makeText(this, "Press again to exit",Toast.LENGTH_SHORT).show();
        }
        backPressedTime = System.currentTimeMillis();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(countDownTimer != null){
            countDownTimer.cancel();
        }
    }
}
