package szewczyk.pum.lista;

import android.content.Context;
import android.database.Cursor;
import android.graphics.Color;
import android.view.View;
import android.view.ViewGroup;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class EntrySimpleCursorAdapter extends SimpleCursorAdapter {
    public EntrySimpleCursorAdapter(Context context, int layout, Cursor c, String[] from, int[] to, int flags){
        super(context, layout, c, from, to, flags);
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        return super.newView(context, cursor, parent);
    }

    public void bindView(View view, Context context, Cursor cursor){
        super.bindView(view, context, cursor);

        class ViewHolder{
            int id;
            boolean check;
            String text;
            View listTab;
        }

        ViewHolder holder = (ViewHolder) view.getTag();
        if(holder == null){
            holder = new ViewHolder();
            holder.id = cursor.getInt(ListDbAdapter.INDEX_ID);
            holder.check = cursor.getInt(ListDbAdapter.INDEX_CHECKED) == 1 ? true : false;
            holder.text = cursor.getString(ListDbAdapter.INDEX_CONTENT);
            holder.listTab = view.findViewById(R.id.textViewRow); //? textView
            view.setTag(holder);
        }

        int cursorPositionID = cursor.getPosition();
        if(cursorPositionID % 2 != 0){
            ((View)holder.listTab.getParent()).setBackgroundColor(Color.parseColor("#a6ccff"));
            ((TextView)holder.listTab.findViewById(R.id.textViewRow)).setTextColor(Color.parseColor("#161617")); // textView ??
        }
        else {
            ((View)holder.listTab.getParent()).setBackgroundColor(Color.parseColor("#7ab3ff"));
            ((TextView)holder.listTab.findViewById(R.id.textViewRow)).setTextColor(Color.parseColor("#161617")); // textView ??
        }



    }
}
