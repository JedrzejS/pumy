package szewczyk.pum.lista;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.database.Cursor;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ListView lv;
    private EditText et;
    private Button but;
    private ArrayAdapter<String> mAdapter;
    private ListDbAdapter mDbAdapter;
    private EntrySimpleCursorAdapter mCursorAdapter;

    /*  TODO
    * ->  Dodawanie do listy i check działa
    * Poprawić:
    *   - Dodać przekreślanie tekstu
    *   - Poprawić kolory żeby nie wyglądały jak kupa
    *   - Po kliknieciu zmiana w Db
    *   - Klikanie calego wiersza w celu checkowania
    *   - Dodać opcje edycji i usuniecia - LongClick i AlertDialog
    *   - Poprawić miejsce na wpisywanie i przycisk Add (float right)
    * */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        lv = findViewById(R.id.firstListView);
        et = findViewById(R.id.firstEditText);
        but = findViewById(R.id.firstButton);

        mDbAdapter = new ListDbAdapter(this);
        mDbAdapter.open();
        Cursor cursor = mDbAdapter.fetchAllEntries();

        String[] from = new String[] {ListDbAdapter.COL_CONTENT};
        int[] to = new int[] {R.id.textViewRow};

        mCursorAdapter = new EntrySimpleCursorAdapter(MainActivity.this, R.layout.rowstyle, cursor, from, to, 0);

        mAdapter = new ArrayAdapter<>(this, R.layout.rowstyle, R.id.textViewRow);
        //lv.setAdapter(mAdapter);
        lv.setAdapter(mCursorAdapter);

        but.setOnClickListener(
            new View.OnClickListener(){
                public void onClick(View v) {
                    String item = et.getText().toString(); // pobieranie tekstu z EditText
                    //mAdapter.add(item); // dodanie punktu do listy z tekstem item
                    //mAdapter.notifyDataSetChanged();
                    mDbAdapter.createList(item, false);

                    Cursor c = mDbAdapter.fetchAllEntries();
                    mCursorAdapter.changeCursor(c);

                    et.setText(""); // zerowanie tekstu w EditText
                }
            }
        );
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if(id == R.id.menuID){
            Toast.makeText(getApplicationContext(),
                    getString(R.string.app_name),
                    Toast.LENGTH_LONG).show();
            return true;
        }
        if(id == R.id.clearButton){
            mAdapter.clear();
            mAdapter.notifyDataSetChanged();
        }
        return super.onOptionsItemSelected(item);
    }



}
