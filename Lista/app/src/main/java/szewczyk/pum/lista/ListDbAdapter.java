package szewczyk.pum.lista;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class ListDbAdapter {

    // region vals & init

    public static final String COL_ID = "_id";
    public static final String COL_CONTENT = "content";
    public static final String COL_CHECKED = "checked";

    public static final int INDEX_ID = 0;
    public static final int INDEX_CONTENT = INDEX_ID + 1;
    public static final int INDEX_CHECKED = INDEX_CONTENT + 1;
    private static final String TAG = "ListDbAdapter";
    private static final String DATABASE_NAME = "dba_shopping";
    private static final int DATABASE_VERSION = 1;
    private static final String TABLE_NAME = "tbl_shopping";
    private static final String DATABASE_CREATE = "CREATE TABLE if not exists "
            + TABLE_NAME + " ( "
            + COL_ID + " INTEGER PRIMARY KEY autoincrement, "
            + COL_CONTENT + " TEXT, "
            + COL_CHECKED + " INTEGER DEFAULT 0);";

    public Context context;
    public DatabaseHelper mDbHelper;
    public SQLiteDatabase mDb;

    ListDbAdapter(Context con){
        context = con;
    }

    // endregion

    public class DatabaseHelper extends SQLiteOpenHelper{
        DatabaseHelper(Context context){
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(DATABASE_CREATE);
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
            onCreate(db);
        }
    }

    // region Db Basics

    public void open() throws SQLException{
        mDbHelper = new DatabaseHelper(context);
        mDb = mDbHelper.getWritableDatabase();
    }
    public void close() {
        if(mDbHelper != null) {
            mDbHelper.close();
        }
    }

    // endregion

    public class Entry{
        public int mId;
        public String mText;
        public boolean mChecked;

        void mId_set(int id){
            mId = id;
        }
        void mText_set(String text){
            mText = text;
        }
        void mChecked_set(boolean checked){
            mChecked = checked;
        }
        int mId_get(){
            return mId;
        }
        String mText_get(){
            return mText;
        }
        boolean mChecked_get(){
            return mChecked;
        }
        Entry(int id, String text, boolean checked){
            mId = id;
            mText = text;
            mChecked = checked;
        }
    }

    // region Entry creation

    public long createEntry(Entry entry){
        Log.w(TAG, "Create entry with id: "+entry.mId_get());
        ContentValues values = new ContentValues();
        values.put(COL_CONTENT, entry.mText_get());
        values.put(COL_CHECKED, entry.mChecked_get());
        return mDb.insert(TABLE_NAME, null, values);
    }


    public long createList(String text, boolean bool){
        ContentValues values = new ContentValues();
        values.put(COL_CONTENT, text);
        values.put(COL_CHECKED, bool);
        return mDb.insert(TABLE_NAME, null, values);
    }
    // endregion

    // region Entry fetching

    public Entry fetchEntryById(int id){
        Log.w(TAG, "Fetch entry with id: "+id);
        Cursor cursor = mDb.query(TABLE_NAME, new String[] {COL_ID, COL_CONTENT, COL_CHECKED},
                COL_ID + "=?",
                new String[]{String.valueOf(id)}, null, null, null);
        if(cursor != null)
            cursor.moveToFirst();

        return new Entry(cursor.getInt(INDEX_ID), cursor.getString(INDEX_CONTENT), cursor.getInt(INDEX_CHECKED) == 1 ? true: false);
    }

    public Cursor fetchAllEntries(){
        Cursor cursor = mDb.rawQuery("SELECT * FROM "+TABLE_NAME, null);
        cursor.moveToFirst();
        return cursor;
    }
    // endregion

    // region Entry update

    public void updateEntry(Entry entry){
        Log.w(TAG, "Update entry with id: "+entry.mId_get());
        ContentValues values = new ContentValues();
        values.put(COL_CONTENT, entry.mText_get());
        values.put(COL_CHECKED, entry.mChecked_get());

        mDb.update(TABLE_NAME, values, COL_ID + "=?", new String[] {String.valueOf(entry.mId_get())});
    }

    //endregion

    // region Entries delete

    public void deleteEntry(int id){
        Log.w(TAG, "Delete entry with id: "+id);
        mDb.delete(TABLE_NAME, COL_ID + "=?", new String[] {String.valueOf(id)});
    }

    public void deleteAllEntries() {
        Log.w(TAG, "All entries delete");
        mDb.delete(TABLE_NAME, null, null);
    }
    // endregion
}
